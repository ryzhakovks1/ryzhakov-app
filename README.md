# Дипломное приложение Нетология

## Структура проекта

```
ryzhakov-app
├── .gitlab-ci.yml
├── diploma-manifest.yaml.tmp
├── Dockerfile
├── html
│   ├── favicon-color.png
│   ├── favicon.png
│   └── index.html
└── README.md
```
### Описание файлов:
- `.gitlab-ci.yml`: Файл настройки CI\CD пайплайна.
- `diploma-manifest.yaml.tmp`: Манифесты Kubernetes для развертывания и обслуживания приложения в кластере (файл c переменными).
- `Dockerfile`: Манифест сборки Docker-образа.
- `README.md`: Документация проекта.
- `html/`: Папка с контентом для веб-приложением.
  - `favicon-color.png`: Иконка для веб-приложения (цветная).
  - `favicon.png`: Иконка для веб-приложения.
  - `index.html`: Основной HTML-файл веб-приложения.

## Ручной запуск в Kubernetes

Для развертывания приложения в Kubernetes, выполните следующие шаги:

```bash
# 1. Соберите Docker-образ приложения с использованием Dockerfile
docker build -t devops-diploma-app:latest .

# 2. Опубликуйте Docker-образ в вашем реестре контейнеров
docker tag devops-diploma-app:latest cr.yandex/$REGISTRY_ID/devops-diploma-app:$DOCKER_IMAGE_TAG
docker push cr.yandex/$REGISTRY_ID/devops-diploma-app:$DOCKER_IMAGE_TAG

# 3. Подставьте значения в манифест
envsubst < diploma-manifest.yaml.tmp > diploma-manifest.yaml

# 4. Примените манифесты Kubernetes для развертывания приложения
kubectl apply -f diploma-manifest.yaml
```

Подождите, пока развертывание завершится, и затем проверьте доступность вашего приложения по адресу http://YourNodeIP:30080.

## Описание CI/CD Пайплайна


### 1. Этап `build`

На этом этапе происходит сборка Docker-образа приложения. Пайплайн использует Docker-контейнер с версией 25.0.3. Важные шаги на этом этапе:

- Установка git в контейнере.
- По умолчанию сборка Docker-образа приложения с тегом `$CI_JOB_ID`.
- При создании git tag Docker-образ соберется с тегом `$CI_COMMIT_TAG`
- Отправка Docker-образа в реестр контейнеров cr.yandex

```yaml
build:
  before_script:
    - apk add --update --no-cache git
  rules:
    - if: '$CI_COMMIT_TAG'
      variables:
        DOCKER_IMAGE_TAG: $CI_COMMIT_TAG
    - if: $CI_PIPELINE_SOURCE == "push"
  stage: build
  script:
    - docker build -t cr.yandex/$REGISTRY_ID/devops-diploma-app:$DOCKER_IMAGE_TAG .
    - docker push cr.yandex/$REGISTRY_ID/devops-diploma-app:$DOCKER_IMAGE_TAG
```

### 2. Этап `deploy`

Этап запускается при создании git tag.

На этом этапе приложение разворачивается в Kubernetes. Основные шаги включают:
- Выбор контекста Kubernetes.
- Подстановка значений в манифест из файла `diploma-manifest.yaml.tmp`.
- Применение манифеста для развертывания приложения в Kubernetes.

```yaml
deploy:
  rules:
    - if: '$CI_COMMIT_TAG'
      variables:
        DOCKER_IMAGE_TAG: $CI_COMMIT_TAG
  image: 
    name: cr.yandex/$REGISTRY_ID/kubectl:1.29.2-debian-12-r3-gettext
  stage: deploy
  script:
    - kubectl config get-contexts
    - kubectl config use-context ryzhakovks1/ryzhakov-app:ryzhakovks-diploma
    - envsubst < diploma-manifest.yaml.tmp > diploma-manifest.yaml
    - kubectl apply -f diploma-manifest.yaml
```



## Зависимости и Технологии
- Docker
- Kubernetes
- Nginx

## Лицензия
Этот проект распространяется под лицензией MIT. Подробности см. в файле [LICENSE](./LICENSE).


